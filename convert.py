import re
import yaml

def convert_submodules(input_text):
    output = {}
    submodule_pattern = re.compile(r'\[submodule "(.*)"\]\n\s+path = (.*)\n\s+url = (.*)\n\s+branch = (.*)\n')
    matches = submodule_pattern.findall(input_text)
    
    for match in matches:
        submodule_name, path, url, branch = match
        extension_name = path.split('/')[-1]
        output[extension_name] = {
            'path': path,
            'repo_url': url,
            'branch': branch
        }
    
    return output

def write_result(output_file, result):
    with open(output_file, 'w') as f:
        yaml.dump(result, f, default_flow_style=False)

input_file = "submodules.txt"
output_file = "extensions.yaml"

with open(input_file, 'r') as f:
    input_text = f.read()

result = convert_submodules(input_text)
write_result(output_file, result)
